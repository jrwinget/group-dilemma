# group-dilemma

# Individual and group morality: An extension of the trolley problem
## Jeremy R. Winget (jwinget@luc.edu), R. Scott Tindale

This project examines how group membership and moral dilemma type influence moral decision-making. We manipulated group membership by randomly assigning participants to either an individual or two-person group condition, and moral dilemma type by using the trolley dilemma paradigm used in previous psychological research (e.g., Bartels, 2008; Greene et al., 2001, 2004; Koenigs, Young, Adolphs, Tranel, Cushman, Hauser, & Damasio, 2007; Suter & Hertwig, 2011; Valdesolo & DeSteno, 2006). Results indicate a main effect of dilemma type on participants’ judgments to each moral dilemma. But, there were no significant results of group membership or the interaction of the independent variables on participant judgments. We discuss some problematic features of the traditional trolley paradigm and suggest possible solutions to these limitations.